local chamada = 0
local registrador = {}
local hora = "H_".. os.date("%H") .. "-" .. os.date("%M") .. "-"..os.date("%S")
local data = "D_" .. os.date("%d") .. "-" .. os.date("%m") 
local fileName =  "LOG" .. "-"..data .. "-" .. hora .. ".md"

local registros ={}

function registrador.evento(texto)
    local quebraLinha = "\n"
    local evento = texto .. quebraLinha 
    table.insert( registros,evento )
    print(evento)
end
function registrador.addChamada()
    chamada = chamada + 1
    return tostring(chamada)
end

function registrador.salvarRegistro()
    local file = io.open(fileName,"w")
    for i = 1,#registros do
        file:write(registros[i],"\n")
        i = i + 1
    end
    file:close()
end
return registrador