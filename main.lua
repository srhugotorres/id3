--[[]]--
local registrador = require "id3.registrador"
registrador.evento("## " .. "INICIO DA EXECUÇÃO")
--
local id3 = require "id3.induzirArvore"
local dados = require "id3.dados.criadorDeDados"
--
local conjuntoExemplo = dados.getTabelaCredito()
local propriedades = {}
    propriedades[1] = "HistoricoCredito"
    propriedades[2] = "Divida"
    propriedades[3] = "Garantia"
    propriedades[4] = "Renda"

local classe = "Risco"
--
print("\n")

id3.run(conjuntoExemplo,propriedades,classe)
registrador.salvarRegistro()