local novoNo = require "id3.arvore.no"
local registrador = require "id3.registrador"
local arvore = {}
function arvore.new()
    local instance = {}
    local ultimoRamo
    instance.noRaiz = nil
    registrador.evento("* ".."árvore criada!")
    function instance.criarNo(valor)
        local noNovo = novoNo.new(valor)
        --registrador.evento("No" .. "'" .. tostring(valor) .. "'" .." criado!")
        if instance.noRaiz == nil then
            instance.noRaiz = noNovo
            registrador.evento("* " .. "No raiz: ".. "'".. tostring(valor).."'")
        else
            --adiciona o nó a um ramo já criado
            registrador.evento("* ".."no: ".. valor .. " no ramo: " .. ultimoRamo)
            instance.noRaiz.ramos[ultimoRamo] = noNovo


        end
        return noNovo
    end
    function instance.adicionarRamo(valor)
        ultimoRamo = valor
        instance.noRaiz.ramos[valor] = nil
        registrador.evento("* ".."Ramo " .."'" .. tostring(valor) .."'".. " criado!")
        return instance.noRaiz.ramos[valor]
    end

    return instance

end
return arvore
