local registrador = require "id3.registrador"
local no = {}
function no.new(valor)
    local instance = {}
    instance.valor = valor
    instance.ramos ={}
    registrador.evento("* ".."No " .."'" .. tostring(valor) .."'".. " criado!")
    return instance
end
return no