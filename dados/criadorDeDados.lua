local novoItem = require "id3.dados.dadosCreditos"


local tabelaCredito = {}
-- Criando itens
tabelaCredito[1] = novoItem.newItem(
    "1",
    "alto",
    "ruim",
    "alta",
    "nenhuma",
    "$0 a $15 mil"
)
tabelaCredito[2] = novoItem.newItem(
    "2",
    "alto",
    "desconhecida",
    "alta",
    "nenhuma",
    "$15 a $35 mil"
)
tabelaCredito[3] = novoItem.newItem(
    "3",
    "moderado",
    "desconhecida",
    "baixa",
    "nenhuma",
    "$15 a $35 mil"
)
tabelaCredito[4] = novoItem.newItem(
    "4",
    "alto",
    "desconhecida",
    "baixa",
    "nenhuma",
    "$0 a $15 mil"
)
tabelaCredito[5] = novoItem.newItem(
    "5",
    "baixo",
    "desconhecida",
    "baixa",
    "nenhuma",
    "acima de $35 mil"
)
tabelaCredito[6] = novoItem.newItem(
    "6",
    "baixo",
    "desconhecida",
    "baixa",
    "adequada",
    "acima de $35 mil"
)
tabelaCredito[7] = novoItem.newItem(
    "7",
    "alto",
    "ruim",
    "baixa",
    "nenhuma",
    "$0 a $15 mil"
)
tabelaCredito[8] = novoItem.newItem(
    "8",
    "moderado",
    "ruim",
    "baixa",
    "adequada",
    "acima de $35 mil"
)
tabelaCredito[9] = novoItem.newItem(
    "9",
    "baixo",
    "boa",
    "baixa",
    "nenhuma",
    "acima de $35 mil"
)
tabelaCredito[10] = novoItem.newItem(
    "10",
    "baixo",
    "boa",
    "alta",
    "adequada",
    "acima de $35 mil"
)
tabelaCredito[11] = novoItem.newItem(
    "11",
    "alto",
    "boa",
    "alta",
    "nenhuma",
    "$0 a $15 mil"
)
tabelaCredito[12] = novoItem.newItem(
    "12",
    "moderado",
    "boa",
    "alta",
    "nenhuma",
    "$15 a $35 mil"
)
tabelaCredito[13] = novoItem.newItem(
    "13",
    "baixo",
    "boa",
    "alta",
    "nenhuma",
    "acima de $35 mil"
)
tabelaCredito[14] = novoItem.newItem(
    "14",
    "alto",
    "ruim",
    "alta",
    "nenhuma",
    "$15 a $35 mil"
)

local criadorDeDados = {}
function criadorDeDados.getListaString(tabela)
    local tabelaTexto= "{ }"

    for i = 1,#tabela do
        if i == 1 then
            tabelaTexto= "{ "
            tabelaTexto = tabelaTexto .. tabela[1] .. ","
            i = i + 1
        elseif i == #tabela then
            tabelaTexto = tabelaTexto .. tabela[#tabela] .. " }"
            break
        else
            tabelaTexto = tabelaTexto .. tabela[i] .. ","
            i = i + 1
        end

    end
    return tabelaTexto
end
function criadorDeDados.getTabelaCredito()
    return tabelaCredito
end
function criadorDeDados.tabelaCredito()
    return tabelaCredito
end
function criadorDeDados.percorreColuna(tabela,coluna)
    local valoresColuna = {}
    for i =1,#tabela do
        table.insert( valoresColuna,tabela[i][coluna])
    end
    return valoresColuna
end
function criadorDeDados.temMesmoValor(tabela,classe)
    local tabelaLocal = criadorDeDados.percorreColuna(tabela,classe)
    local classeReferencia = tabelaLocal[1]
    for i = 2,#tabela do
        if classeReferencia ~= tabelaLocal[i] then
            return false
        else
            i = i + 1
        end
    end
    return true
end
function criadorDeDados.criarParticao(coluna,valor)
    local particao = {}
    for i = 1,#tabelaCredito do
        if tabelaCredito[i][coluna] == valor then
            table.insert( particao, tabelaCredito[i] )
            i = i + 1
        else
            i = i + 1
        end
    end
    return particao
end
--[[
]]--
return criadorDeDados