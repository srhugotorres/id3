local dadosCreditos ={}
function dadosCreditos.newItem(
    indice,
    risco,
    historico,
    divida,
    garantia,
    renda
)
    local instance = {}
    instance["Indice"] = indice
    instance["Risco"] = risco
    instance["HistoricoCredito"] = historico
    instance["Divida"] = divida
    instance["Garantia"] = garantia
    instance["Renda"] = renda
    return instance
end

return dadosCreditos