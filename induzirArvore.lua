local novoNo = require "id3.arvore.no"
local novaArvore = require "id3.arvore.arvore"
local dados = require "id3.dados.criadorDeDados"
local registrador = require "id3.registrador"
--
local arvore = novaArvore.new()

local induzirArvore = {}

function induzirArvore.run(conjuntoExemplo,propriedades,classe)
    local conjuntoExemploTexto = dados.getListaString(
        dados.percorreColuna(
            conjuntoExemplo,
            "Indice"
        )
    )
    local propriedadesTexto = dados.getListaString(propriedades)
    --
    registrador.evento("\n".."## ".. registrador.addChamada() .. "a ".. "CHAMADA DA " .."FUNÇÃO INDUZIR ARVORE")
    registrador.evento("* ".."conjunto exemplo é: " .. conjuntoExemploTexto)
    registrador.evento("* ".."Propriedades: " .. propriedadesTexto)
    registrador.evento("* ".."A classe é: " .. classe)
    --
    if dados.temMesmoValor(conjuntoExemplo,classe) then
        --
        registrador.evento("\n".."1 - Todos os elementos do conjunto são da mesma classe? - Sim" .. "\n")
        registrador.evento("### Primeiro IF")
        --
        return novoNo.new(classe)
    elseif next(propriedades) == nil then
        --
        registrador.evento("\n".."1 - Todos os elementos do conjunto são da mesma classe? - Não".."\n" ..
        "\n".."2 - Propriedade é vázio? - Sim".."\n")
        registrador.evento("### Segundo IF")
        --
        return novoNo.new(conjuntoExemploTexto)
    else
        registrador.evento("\n".."1 - Todos os elementos do conjunto são da mesma classe? - Não" ..
        "\n".."2 - Propriedade é vázio? - Não" .. "\n"..
        "\n" .. "3 - Else ..." .. "\n")
        registrador.evento("### Else")
        local propriedadeP = propriedades[1]
        local no = arvore.criarNo(
            propriedadeP .. " ?"
        )
        table.remove(
            propriedades,
            1
        )

        local colunaPropriedadeP = dados.percorreColuna(
            conjuntoExemplo,
            propriedadeP
        )
        for i = 1, #colunaPropriedadeP do
            local ramo = arvore.adicionarRamo(
                colunaPropriedadeP[i]
            )
            registrador.evento("* ".."Ramo " .."'".. colunaPropriedadeP[i] .."'".. " pertence ao Nó: " .."'".. no.valor.."'")
            local particao  = dados.criarParticao(
                propriedadeP,
                colunaPropriedadeP[i]
            )
            local noResultado = induzirArvore.run(
                particao,
                propriedades,
                classe
            )
            ramo = noResultado
            registrador.evento("* " .. "fim do Else")
            i = i + 1
        end

    end

end

return induzirArvore